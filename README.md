Install vim-plug from https://github.com/junegunn/vim-plug

### After pasting in .vimrc file do below

1. do `:PlugInstall`
2. do `:CocInstall coc-json coc-python coc-snippets coc-vimlsp coc-html coc-css coc-tsserver coc-tslint-plugin`
3. do `:CocConfig`
```
{
  // "coc.preferences.formatOnSaveFiletypes": ["css", "markdown", "javascript", "graphql", "html", "yaml",  "json", "python"],

  // python config
  "python.linting.enabled": true,
  "python.linting.pylintEnabled": true,

  "snippets.ultisnips.directories":
  [
    "UltiSnips",
    "~/.config/nvim/utils/snips"
  ]

}
